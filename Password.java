package com.company;

import java.util.ArrayList;

public class Password {
    // passwordStr // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols

    private String passwordStr;

    public Password(String passwordStr){
        setPasswordStr(passwordStr);
    }

    public void changePassword(User signeduser, String p){
        signeduser.getPassword().setPasswordStr(p);
    }

    public boolean checkPassword(String password){
        for(int i = 0; i<password.length(); i++){
            if(Character.isUpperCase(i)){
                break;
            }
        }
        for(int i = 0; i< password.length();i++){
            if(Character.isLowerCase(i)){
                break;
            }
        }
        for(int i = 0; i<password.length();i++){
            if(Character.isDigit(i)){
                break;
            }
        }
        return true;
    }

    public void setPasswordStr(String passwordStr) {
        this.passwordStr = passwordStr;
    }

    public String getPasswordStr() {
        return passwordStr;
    }

}