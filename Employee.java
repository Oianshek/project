package com.company;


public class Employee extends User{
    private int salary;

    public Employee(){

    }

    public Employee(int salary){
        setSalary(salary);
    }

    public Employee(int salary, String name, String surname,String username, Password password){
        super(name,surname,username,password);
        setSalary(salary);
    }
    public Employee(int id,int salary, String name, String surname,String username, Password password){
        super(id,name,surname,username,password);
        setSalary(salary);
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Employee with salary "+salary+" and with some data like "+ super.toString();
    }
}
