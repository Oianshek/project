package com.company;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApp {
    // users - a list of users
    private Scanner sc = new Scanner(System.in);
    private User signedUser;
    private ArrayList<User> users = new ArrayList<>();
    private ArrayList<Employee> empusers = new ArrayList<>();
    private boolean isBoss = false;
    private boolean isEmp = false;


    private void addUser(User user){
        users.add(user);
    }


    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            }
            else {
                userProfile();
            }
        }
    }

    public void userProfile() {
        while (true) {
            System.out.println("Welcome, SLAVE!");
            System.out.println(signedUser);
            System.out.println("Enter 1 to change password");
            System.out.println("Enter 0, if you want to Log Off.");
            if (sc.nextInt() == 1) {
                System.out.println("Enter your password: ");
                String pass1 = sc.next();
                System.out.println("Enter new password: ");
                String pass2 = sc.next();
                signedUser.getPassword().changePassword(signedUser, pass2);
            } else {
                logOff();
                break;
            }
        }
    }
    public void bossProfile(){
        while(true){
            System.out.println("Welcome, BOSS!");
            System.out.println("If you want to change employee's salary enter 1");
            int ch3 = sc.nextInt();
            if(ch3 == 1){
                System.out.println("Please, choose whose salary you want to change");
                String uschange = sc.next();
                Employee searchemp = new Employee();
                for(Employee employee : empusers){
                    if(uschange.equals(employee.getUsername())){
                        searchemp = employee;
                    }else {
                        System.out.println("There is no user with this username");
                    }
                }
                if(searchemp!=null){
                    System.out.println("Set salary, you need");
                    int sal = sc.nextInt();
                    searchemp.setSalary(sal);
                }else{
                    System.out.println("ERROR 404");
                    break;
                }
            }else{
                break;
            }
        }
    }

    private void logOff() {
        signedUser = null;
    }

    private void authentication() {
        // sign in
        // sign up
        int ch1 = sc.nextInt();
        System.out.println("If you are Administrator, please choose command 1, if you're not, choose command 2");
        if(ch1 == 1){
            System.out.println("Choose a command: ");
            System.out.println("1.If you have an account, please, Sign In");
            System.out.println("2.If you do not have an account, please, Sign Up");
            int ch2 = sc.nextInt();
            if(ch2 == 1){
                signIn();
                isBoss = true;
                bossProfile();
            }else{
                signUp();
            }
        }else{
            System.out.println("Choose a command: ");
            System.out.println("1.If you have an account, please, Sign In");
            System.out.println("2.If you do not have an account, please, Sign Up");
            int ch = sc.nextInt();
            if(ch == 1){
                signIn();
                isEmp = true;

            }
            else{
                signUp();
            }
        }
    }

    private void signIn() {
        System.out.println("Enter your Username: ");
        String usname = sc.next();
        boolean a = false;
        for (User user : users){
            if(usname.equals(user.getUsername())){
                a = true;
                break;
            }
        }
        while(true) {
            if (a) {
                System.out.println("Write your Password: ");
                String pass = sc.next();
                boolean b = false;
                for (User user : users) {
                    if (pass.equals(user.getPassword().getPasswordStr())) {
                        b = true;
                        signedUser = user;
                        break;
                    }
                }
                if (b) {
                    System.out.println("You're signed in!");
                } else {
                    System.out.println("Password is incorrect! Try again.");
                    pass = sc.next();
                }
            }
            break;
        }
    }

    private void signUp() {
        System.out.println("Write your name: ");
        String name = sc.next();
        System.out.println("Write your surname: ");
        String surname = sc.next();
        System.out.println("Write username: ");
        String username = sc.next();
        boolean us = false;
        for (User user : users) {
            if(username.equals(user.getUsername())){
                System.out.println("Write another Username: ");
                username = sc.next();
            }else{
                us = true;
                break;
            }
        }
        System.out.println("Write password: ");
        String password = sc.next();
        Password p = new Password(password);
        if(us){
            if(p.checkPassword(password)){
                User newuser = new User(name,surname,username,p);
                addUser(newuser);
            }
        }
    }


    public void start() throws IOException {
        // fill userlist from db.txt

        File file = new File("C:\\Users\\oians\\IdeaProjects\\Assignment 2\\src\\com\\company\\dbemployee.txt");
        Scanner fsc = new Scanner(file);

        while(fsc.hasNext()){
            int id = fsc.nextInt();
            int salary = fsc.nextInt();
            String name = fsc.next();
            String surname = fsc.next();
            String username = fsc.next();
            Password password = new Password(fsc.next());
            Employee emp = new Employee(id,salary,name,surname,username,password);
            empusers.add(emp);
        }

        for (Employee emp:empusers) {
            User us = new User(emp.getId(),emp.getName(),emp.getSurname(),emp.getUsername(),emp.getPassword());
            users.add(us);
        }

        int k = 0;
        while (fsc.hasNext()){
            int id = fsc.nextInt();
            if(id>k){
                k = id;
            }
            String name = fsc.next();
            String surname = fsc.next();
            String username = fsc.next();
            Password password = new Password(fsc.next());
            User user = new User(id, name, surname, username,password);
            users.add(user);
        }
        users.get(0).setId_gen(k);

        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

        for (User user: users) {
            System.out.println(user+"\n");
        }

        saveUserList();
    }

    public void saveUserList() throws IOException {
        String data = " ";
        for (User user : users) {
            data = data + user.getId() + " " + user.getName() + " " + user.getSurname() + " " + user.getUsername() + " " + user.getPassword().getPasswordStr() + "\n";
        }

        Files.write(Paths.get("C:\\Users\\oians\\IdeaProjects\\Assignment 2\\src\\com\\company\\dbemployee.txt"),data.getBytes());

    }
}
