package com.company;

import java.util.ArrayList;

public class User {
    // id (you need to generate this id by static member variable)
    // name, surname
    // username
    // password
    private static int id_gen;
    private int id;
    private String name;
    private String surname;
    private String username;
    private Password password;

    public User() {
        id = id_gen++;
    }

    public User(String name, String surname, String username, Password password) {
        this();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public User(int id, String name, String surname, String username, Password password) {
        id_gen = id + 1;
        setId(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public void setId_gen(int id) {
        this.id_gen = id + 1;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Password getPassword() {
        return password;
    }


    @Override
    public String toString() {
        return id + " " + name + " " + surname + " " + username + " " + password.getPasswordStr();
    }
}
