package com.company;

public class Boss extends User{
    int salary;

    public Boss (int salary){
        setSalary(salary);
    }

    public Boss(int salary, String name, String surname, String username, Password password){
        super(name,surname,username,password);
        setSalary(salary);
    }


    public void setSalary(int salary) {
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

}
