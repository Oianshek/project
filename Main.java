package com.company;

import java.io.IOException;


public class Main {

    public static void main(String[] args) throws IOException {

        MyApp app = new MyApp();
        app.start();
    }
}
